# Project Description :file_folder:
This project introduces a **Tic-Tac-Toe Game** featuring three distinct difficulty levels: easy, medium, and hard.
To initiate a game, simply enter the command "start "difficulty levels" user". The first player begins the game
with _X_, while the second player uses _O_. When specifying moves, utilize coordinates based on an xy plane ranging
from 1 to 3.

The easy difficulty level employs random moves, making decisions without a strategic approach. The medium difficulty
level attempts to win if a victory is possible in the next move, or blocks the user if the user could win on the
following move. The hard difficulty level leverages the minimax algorithm to make optimal choices by considering all
potential outcomes from each move.

# Stages
 ### :white_check_mark: Stage 1:
Output the specified 3x3 table before the user makes a move.
Request that the user enters the coordinates of the move they wish to make.
The user then inputs two numbers representing the cell in which they wish to place their move. The game always 
starts with X, so the user's move should be made with this symbol if there are an equal number of X's and O's in 
the table. If the table contains an extra X, the move should be made with O.
Analyze the user input and show messages in the following situations:
• This cell is occupied! Choose another one! — if the cell is not empty;
• You should enter numbers! — if the user tries to enter letters or symbols instead of numbers;
• Coordinates should be from 1 to 3! — if the user attempts to enter coordinates outside of the table's range.
Display the table again with the user's most recent move included.
Output the state of the game.

### :white_check_mark: Stage 2:
Display an empty table when the program starts.
The user plays first as X, and the program should ask the user to enter cell coordinates.
Next, the computer makes its move as O, and the players then move in turn until someone wins or the game results in
a draw.
Print the final outcome at the very end of the game.

### :white_check_mark: Stage 3:
Write a menu loop, which can interpret two commands: start and exit.
Implement the command start. Two options are possible for now: user to play as a human and easy to play as an AI.
The exit command should simply end the program.

### :white_check_mark: Stage 4:
AI is playing at medium difficulty level, it makes moves using the following logic:
If it already has two in a row and can win with one further move, it does so.
If its opponent can win with one move, it plays the move necessary to block this.
Otherwise, it makes a random move.

### :white_check_mark: Stage 5:
In this last stage, you need to implement the hard difficulty level using the minimax algorithm. 
