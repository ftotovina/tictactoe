package org.example.tic_tac_toe;

public interface UserStrategy {
    Move getMove(Cell[][] board);
}
