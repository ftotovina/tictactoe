package org.example.tic_tac_toe;

import static org.example.tic_tac_toe.TicTacToe.BOARD_SIZE;

public class Utils {

    public static boolean checkWin(char player, Cell[][]board) {
        for (int i = 0; i < BOARD_SIZE; i++) {
            boolean rowWin = true;
            boolean colWin = true;
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (board[i][j].getValue() != player) {
                    rowWin = false;
                }
                if (board[j][i].getValue() != player) {
                    colWin = false;
                }
            }
            if (rowWin || colWin) {
                return true;
            }
        }

        boolean mainDiagonalWin = true;
        boolean antiDiagonalWin = true;
        for (int i = 0; i < BOARD_SIZE; i++) {
            if (board[i][i].getValue() != player) {
                mainDiagonalWin = false;
            }
            if (board[i][BOARD_SIZE - 1 - i].getValue() != player) {
                antiDiagonalWin = false;
            }
        }
        return mainDiagonalWin || antiDiagonalWin;
    }

    public static boolean isBoardFull(Cell[][]board) {
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (board[i][j].getValue() == ' ') {
                    return false;
                }
            }
        }
        return true;
    }
}
