package org.example.tic_tac_toe;

import java.util.Random;

public class EasyUserStrategy implements UserStrategy {

    @Override
    public Move getMove(Cell[][] board) {
        Random random = new Random();
        int row, col;
        do {
            row = random.nextInt(board.length);
            col = random.nextInt(board[0].length);
        } while (board[row][col].getValue() != ' ');

        return new Move(row, col);
    }
}
