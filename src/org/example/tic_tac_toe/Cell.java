package org.example.tic_tac_toe;

public class Cell {
    private char value;
    private final int row;
    private final int column;

    public Cell(char value, int row, int column) {
        this.value = value;
        this.row = row;
        this.column = column;
    }

    public char getValue() {
        return value;
    }

    public void setValue(char value) {
        this.value = value;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
