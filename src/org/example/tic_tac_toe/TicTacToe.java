package org.example.tic_tac_toe;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

import static org.example.tic_tac_toe.Utils.checkWin;
import static org.example.tic_tac_toe.Utils.isBoardFull;

public class TicTacToe {

    public static final char X = 'X';
    public static final char O = '0';
    public static final int BOARD_SIZE = 3;
    private static final String EASY_USER = "start easy user";
    private static final String MEDIUM_USER = "start medium user";
    private static final String HARD_USER = "start hard user";
    private final Scanner scanner;
    private Cell[][] board;


    public TicTacToe() {
        board = new Cell[BOARD_SIZE][BOARD_SIZE];
        scanner = new Scanner(System.in);
    }

    public void createBoard() {
        Cell[][] cells = new Cell[BOARD_SIZE][BOARD_SIZE];
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                cells[i][j] = new Cell(' ', i, j);
            }
        }

        this.board = cells;
    }

    public void playGame() {
        Map<String, UserStrategy> userStrategies = createStrategyMap();
        boolean gameEnds = false;
        while (!gameEnds) {
            System.out.print("Input command: ");
            String command = scanner.nextLine();
            if (command.equals("exit")) {
                gameEnds = true;
            } else if (userStrategies.containsKey(command)) {
                createBoard();
                UserStrategy userStrategy = userStrategies.get(command);
                playUserStrategy(userStrategy);
            } else {
                System.out.println("Bad Parameters");
            }


        }
    }

    private Map<String, UserStrategy> createStrategyMap() {
        Map<String, UserStrategy> userStrategies = new HashMap<>();
        userStrategies.put(EASY_USER, new EasyUserStrategy());
        userStrategies.put(MEDIUM_USER, new MediumUserStrategy());
        userStrategies.put(HARD_USER, new HardUserStrategy());
        return userStrategies;
    }


    private void playUserStrategy(UserStrategy strategy) {
        boolean gameStops = false;
        boolean personTurn = false;
        while (!gameStops) {
            printBoard();
            Move move;
            if (!personTurn) {
                move = getPersonMove();
                personTurn = true;
            } else {
                move = strategy.getMove(board);
                personTurn = false;
            }
            makeMove(move.getRow(), move.getColumn(), personTurn);
            gameStops = checkWinner();
        }
        printBoard();
    }

    private boolean checkWinner() {
        if (checkWin(X,board)) {
            System.out.println(X + " wins");
            return true;
        } else if (checkWin(O,board)) {
            System.out.println(O + " wins");
            return true;
        } else if (isBoardFull(board)) {
            System.out.println("Draw");
            return true;
        }
        return false;
    }

    private Move getPersonMove() {
        Move move = null;
        while (move == null || !checkPersonMove(move.getRow(), move.getColumn())) {
            move = getPlayerMove();
        }
        return move;

    }

    private void printBoard() {
        System.out.println("---------");
        for (Cell[] chars : board) {
            System.out.print("| ");
            for (Cell cell : chars) {
                System.out.print(cell + " ");
            }
            System.out.println("|");
        }
        System.out.println("---------");
    }

    private Move getPlayerMove() {
        try {
            System.out.print("Enter row from 1-" + BOARD_SIZE + ": ");
            int row = scanner.nextInt() - 1;
            System.out.print("Enter column from 1-" + BOARD_SIZE + ": ");
            int column = scanner.nextInt() - 1;
            scanner.nextLine();
            return new Move(row, column);
        } catch (InputMismatchException e) {
            System.out.println("You should enter numbers!");
            scanner.nextLine();
            return null;
        }
    }

    private boolean checkPersonMove(int row, int column) {
        if (row < 0 || row >= BOARD_SIZE || column < 0 || column >= BOARD_SIZE) {
            System.out.println("Coordinates should be from 1 to " + BOARD_SIZE + " !");
            return false;
        }
        if (board[row][column].getValue() != ' ') {
            System.out.println("This cell is occupied! Choose another one!");
            return false;
        }
        return true;
    }

    private void makeMove(int row, int column, boolean personTurn) {
        if (personTurn) {
            board[row][column].setValue(X);
        } else {
            board[row][column].setValue(O);
        }
    }
}
