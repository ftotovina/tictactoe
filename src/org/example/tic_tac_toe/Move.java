package org.example.tic_tac_toe;

public class Move {

    private int row;
    private int column;

    public Move() {

    }

    public Move(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getColumn() {
        return column;
    }

    public int getRow() {
        return row;
    }
}
