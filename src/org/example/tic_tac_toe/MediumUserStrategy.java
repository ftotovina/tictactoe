package org.example.tic_tac_toe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static java.util.Objects.isNull;
import static org.example.tic_tac_toe.TicTacToe.BOARD_SIZE;
import static org.example.tic_tac_toe.TicTacToe.X;

public class MediumUserStrategy extends EasyUserStrategy implements UserStrategy {


    @Override
    public Move getMove(Cell[][] board) {
        Move move = getWinningMove(board);
        if (move != null) {
            return move;
        }
        return super.getMove(board);
    }

    private Cell getEmptyCell(List<Cell> cells) {
        Cell emptyCell = null;
        int personMoves = 0;

        for (Cell cell : cells) {
            if (cell.getValue() == X) {
                personMoves++;
            } else if (cell.getValue() == ' ') {
                emptyCell = cell;
            }
        }
        if (personMoves == BOARD_SIZE - 1 && emptyCell != null) {
            return emptyCell;
        }
        return null;
    }

    private Move getWinningMove(Cell[][] board) {
        Move winningMove;
        if ((winningMove = verifyFirstDiagonal(board)) != null) {
            return winningMove;
        }
        if ((winningMove = verifySecondDiagonal(board)) != null) {
            return winningMove;
        }
        if ((winningMove = verifyColumn(board)) != null) {
            return winningMove;
        }
        return verifyRow(board);
    }

    private Move verifyFirstDiagonal(Cell[][] board) {
        List<Cell> firstDiagonalCells = new ArrayList<>(BOARD_SIZE);
        for (int i = 0; i < BOARD_SIZE; i++) {
            firstDiagonalCells.add(board[i][i]);
        }
        Cell winningMoveFirstDiagonal = getEmptyCell(firstDiagonalCells);
        if (!isNull(winningMoveFirstDiagonal)) {
            return new Move(winningMoveFirstDiagonal.getRow(), winningMoveFirstDiagonal.getColumn());
        }
        return null;
    }

    private Move verifySecondDiagonal(Cell[][] board) {
        List<Cell> secondDiagonalCells = new ArrayList<>(BOARD_SIZE);
        for (int i = 0; i < BOARD_SIZE; i++) {
            secondDiagonalCells.add(board[i][BOARD_SIZE - 1 - i]);
        }
        Cell winningMoveSecondDiagonal = getEmptyCell(secondDiagonalCells);
        if (!isNull(winningMoveSecondDiagonal)) {
            return new Move(winningMoveSecondDiagonal.getRow(), winningMoveSecondDiagonal.getColumn());
        }
        return null;
    }

    private Move verifyRow(Cell[][] board) {
        for (int i = 0; i < BOARD_SIZE; i++) {
            List<Cell> rowCells = new ArrayList<>(BOARD_SIZE);
            for (int j = 0; j < BOARD_SIZE; j++) {
                rowCells.add(board[j][i]);
            }

            Cell winningMove = getEmptyCell(rowCells);
            if (winningMove != null) {
                return new Move(winningMove.getRow(), winningMove.getColumn());
            }
        }
        return null;
    }

    private Move verifyColumn(Cell[][] board) {
        for (int i = 0; i < BOARD_SIZE; i++) {
            List<Cell> columnCells = new ArrayList<>(BOARD_SIZE);
            columnCells.addAll(Arrays.asList(board[i]).subList(0, BOARD_SIZE));

            Cell winningMove = getEmptyCell(columnCells);
            if (winningMove != null) {
                return new Move(winningMove.getRow(), winningMove.getColumn());
            }
        }
        return null;
    }
}
