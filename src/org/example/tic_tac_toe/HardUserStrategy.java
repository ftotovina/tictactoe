package org.example.tic_tac_toe;

import static org.example.tic_tac_toe.TicTacToe.BOARD_SIZE;
import static org.example.tic_tac_toe.TicTacToe.O;
import static org.example.tic_tac_toe.TicTacToe.X;
import static org.example.tic_tac_toe.Utils.checkWin;
import static org.example.tic_tac_toe.Utils.isBoardFull;

public class HardUserStrategy implements UserStrategy {

    @Override
    public Move getMove(Cell[][] board) {
        int bestScore = Integer.MIN_VALUE;
        Move bestMove = new Move(-1, -1);

        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (board[i][j].getValue() == ' ') {
                    board[i][j].setValue(O);
                    int score = minimax(O, false, board);
                    board[i][j].setValue(' ');

                    if (score > bestScore) {
                        bestScore = score;
                        bestMove = new Move(i, j);
                    }
                }
            }
        }
        return bestMove;
    }

    private int minimax(int depth, boolean isMaximizingPlayer, Cell[][] board) {
        if (checkWin(X,board)) {
            return -1;
        }
        if (checkWin(O,board)) {
            return 1;
        }
        if (isBoardFull(board)) {
            return 0;
        }
        int bestScore;
        if (isMaximizingPlayer) {
            bestScore = Integer.MIN_VALUE;
            for (int i = 0; i < BOARD_SIZE; i++) {
                for (int j = 0; j < BOARD_SIZE; j++) {
                    if (board[i][j].getValue() == ' ') {
                        board[i][j].setValue(O);
                        int score = minimax(depth + 1, false, board);
                        board[i][j].setValue(' ');
                        bestScore = Math.max(score, bestScore);
                    }
                }
            }
        } else {
            bestScore = Integer.MAX_VALUE;
            for (int i = 0; i < BOARD_SIZE; i++) {
                for (int j = 0; j < BOARD_SIZE; j++) {
                    if (board[i][j].getValue() == ' ') {
                        board[i][j].setValue(X);
                        int score = minimax(depth + 1, true, board);
                        board[i][j].setValue(' ');
                        bestScore = Math.min(score, bestScore);
                    }
                }
            }
        }
        return bestScore;
    }


}
