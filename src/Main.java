import org.example.tic_tac_toe.TicTacToe;

public class Main {
    public static void main(String[] args) {
        TicTacToe ticTacToe = new TicTacToe();
        ticTacToe.playGame();
    }
}
